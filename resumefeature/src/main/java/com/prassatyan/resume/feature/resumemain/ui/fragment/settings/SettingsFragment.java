package com.prassatyan.resume.feature.resumemain.ui.fragment.settings;

import android.os.Bundle;

import com.prassatyan.resume.feature.resumemain.R;

import androidx.fragment.app.Fragment;
import androidx.preference.PreferenceFragmentCompat;
import androidx.preference.PreferenceScreen;

/**
 * @author Prasannajeet Pani
 * Settings screen to display preferences in
 */
public class SettingsFragment extends PreferenceFragmentCompat implements PreferenceFragmentCompat.OnPreferenceStartScreenCallback {

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.app_preferences);
    }

    @Override
    public Fragment getCallbackFragment() {
        return this;
    }

    @Override
    public boolean onPreferenceStartScreen(PreferenceFragmentCompat fragmentCompat, PreferenceScreen preferenceScreen) {
        fragmentCompat.setPreferenceScreen(preferenceScreen);
        return true;
    }
}