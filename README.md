## Resume Application - Resume, Github and LinkedIn in one Android app
This application has been developed by [Satya Chaitanya Kaikala](https://gitlab.com/satyakaikala) and [Prasannajeet Pani](https://gitlab.com/prasannajeet/) to showcase our resume, Github and LinkedIn profiles to anyone who is interested to look at our professional profiles in form of an Android app.

This application uses Product Flavours from Android gradle plugin to work separately for both our profiles

## License

```
   Copyright 2018 Prasannajeet Pani/Satya Kaikala

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
```